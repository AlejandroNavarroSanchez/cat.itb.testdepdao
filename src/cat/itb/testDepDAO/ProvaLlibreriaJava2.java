package cat.itb.testDepDAO;
import cat.itb.depDAO.Departamento;
import cat.itb.depDAO.DepartamentoDAO;
import cat.itb.depDAO.DepartamentoImpl;

public class ProvaLlibreriaJava2 {

    public static void main(String[] args) {

        DepartamentoDAO depDAO = new DepartamentoImpl();

        //INSERTAR
        Departamento dep1 = new Departamento(17,"NÒMINES", "SEVILLA");
        depDAO.insertarDep(dep1);

        //CONSULTAR
        Departamento dep2 = depDAO.consultarDep(17);
        System.out.printf("Dep: %d, Nom: %s, Loc: %s %n", dep2.getDeptno(), dep2.getDnombre(), dep2.getLoc());

        //MODIFICAR
        dep2.setDnombre("nounom");
        dep2.setLoc("novaloc");
        depDAO.modificarDep(dep2);
        System.out.printf("Dep: %d, Nom: %s, Loc: %s %n", dep2.getDeptno(), dep2.getDnombre(), dep2.getLoc());

        //ELIMINAR
        depDAO.eliminarDep(17);

    }

}